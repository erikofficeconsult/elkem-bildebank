﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ElkemBildebank.Model;
using System.ComponentModel;
using System.Threading;

namespace ElkemBildebank
{
    /// <summary>
    /// Interaction logic for BildebankWPF.xaml
    /// </summary>
    public partial class BildebankWPF : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private List<Item> _sharePointSource;
        private GalleryItem _selectedItem;


        public List<Item> SharePointSource
        {
            get
            {
                return _sharePointSource;
            }

            set
            {
                _sharePointSource = value;
                OnPropertyChanged("SharePointSource");
            }
        }
        public GalleryItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public BildebankWPF()
        {
            InitializeComponent();
            DataContext = this;

            imageList.Visibility = Visibility.Collapsed;

            imageList.BackButton.Click += (o, ea) =>
            {
                //vis treeviewet
                treeView.Visibility = Visibility.Visible;
                //skjul slidelisten
                imageList.Visibility = Visibility.Collapsed;
            };

        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void ShowSlideView()
        {
            //skjul treeviewet
            treeView.Visibility = Visibility.Collapsed;
            //vis slidelisten
            imageList.Visibility = Visibility.Visible;
            imageList.IsLoading = true;
            imageList.FolderTitle = SelectedItem.Name;
            imageList.BackButton.IsEnabled = false;

            Task.Factory.StartNew(() =>
            {
                    //Do some work
                    if (SelectedItem.ImageItems == null)
                    {
                        var data = SelectedItem.Handler.GetImagesFromPath(SelectedItem.Path);
                        SelectedItem.ImageItems = data;
                    }

            }).ContinueWith((task) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    imageList.BackButton.IsEnabled = true;
                });
                imageList.Images = SelectedItem.ImageItems;
                imageList.IsLoading = false;
        });
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                SelectedItem = ((VirtualizingStackPanel)sender).DataContext as GalleryItem;
                ShowSlideView();
            }
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //hvis valg item er en fileitem
            if (e.NewValue.GetType() == typeof(GalleryItem))
            {
                SelectedItem = (GalleryItem)e.NewValue;
            }

        }

        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
