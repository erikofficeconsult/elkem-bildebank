﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Office = Microsoft.Office.Core;
using System.Windows;
using System.Diagnostics;

namespace ElkemBildebank
{
    public partial class ThisAddIn
    {
        Microsoft.Office.Tools.CustomTaskPane cTaskPane;
        private BildebankHost _host;
        private BildebankWPF _bildeBank;
        

        private List<Model.Item> _sharePointSource = new List<Model.Item>();

        protected override Office.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new ElkemRibbon();
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            _host = new BildebankHost();
            _bildeBank = _host.Bildebank;
            InitAddinData();
            //Globals.ThisAddIn.Application.AfterShapeSizeChange += Application_AfterShapeSizeChange;
            //Globals.ThisAddIn.Application.WindowSelectionChange += Application_WindowSelectionChange;
        }

        private void Application_WindowSelectionChange(PowerPoint.Selection Sel)
        {
            if (Sel.Type == PowerPoint.PpSelectionType.ppSelectionShapes)
            {
                PowerPoint.Shape sh = Sel.ShapeRange[1];
                if (sh.Name == "TestShape")
                {
                    sh.LockAspectRatio = Office.MsoTriState.msoFalse;
                    switch (sh.Width > sh.Height)
                    {
                        case true:
                            sh.Height = sh.Width;
                            break;

                        case false: 
                            sh.Width = sh.Height;
                            break;
                        default:

                            break;
                    }
                    sh.LockAspectRatio = Office.MsoTriState.msoTrue;
                }
            }
        }

        private void InitAddinData()
        {

            Task.Factory.StartNew(() =>
            {
                //old source
                //List<Model.Item> _logos = new SharepointHandler("Logos", "https://elkem.sharepoint.com/sites/marketing").GetBildebank();
                //List<Model.Item> _photos = new SharepointHandler("Photo Library", "https://elkem.sharepoint.com/sites/marketing/photo", true).GetBildebank();
                //List<Model.Item> _icons = new SharepointHandler("Our graphic tools", "https://elkem.sharepoint.com/sites/marketing").GetBildebank();
                //_sharePointSource = _logos.Concat(_photos).Concat(_icons).ToList();

                //new sauce
                //List<Model.Item> _logos = new SharepointHandler("Logos", "https://elkem.sharepoint.com/sites/marketing").GetBildebank();
                List<Model.Item> _photos = new SharepointHandler("PowerPoint Assets", "https://elkem.sharepoint.com/sites/marketing").GetBildebank();
                //List<Model.Item> _icons = new SharepointHandler("Our graphic tools", "https://elkem.sharepoint.com/sites/marketing").GetBildebank();
                _sharePointSource = _photos.ToList();
            }).ContinueWith((task) =>
            {

                _bildeBank.SharePointSource = _sharePointSource;
                _bildeBank.LoadingTekst.Visibility = Visibility.Collapsed;

            }, TaskScheduler.FromCurrentSynchronizationContext());

            
        }

        private void Application_AfterShapeSizeChange(PowerPoint.Shape shp)
        {
            try
            {
                PowerPoint.Shape s = Application.ActivePresentation.Slides[1].Shapes.AddTextbox(Office.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 28, 28);
                s.TextFrame.TextRange.Text = "HEI";
                if (shp.Name == "TestShape")
                {
                    if (shp.Width > shp.Height)
                    {
                        shp.Height = shp.Width;
                        shp.LockAspectRatio = Office.MsoTriState.msoTrue;
                    }
                    else if (shp.Width < shp.Height)
                    {
                        shp.Width = shp.Height;
                        shp.LockAspectRatio = Office.MsoTriState.msoTrue;
                    }
                }
            }
            catch (Exception)
            {

                Debug.WriteLine("Fant IKKE shape");

            }
        }

        public void showTaskPane()
        {
            InitTaskPane();
        }


        private void InitTaskPane()
        {
            try
            {
                for (int i = CustomTaskPanes.Count; i > 0; i--)
                {
                    if (CustomTaskPanes[i - 1].Title == "Elkem Imagebank")
                    {
                        CustomTaskPanes.RemoveAt(i - 1);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            _host = new BildebankHost();
            _bildeBank = _host.Bildebank;
            _bildeBank.SharePointSource = _sharePointSource;
            _bildeBank.LoadingTekst.Visibility = Visibility.Collapsed;


            cTaskPane = CustomTaskPanes.Add(_host, "Elkem Imagebank");
            cTaskPane.Width = 355;
            cTaskPane.Visible = true;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
