﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ElkemBildebank.Model
{
    public class ImageItem : Item, INotifyPropertyChanged
    {

        private BitmapSource _image;

        public BitmapSource Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                OnPropertyChanged("Image");
            }
        }

        private bool _isInserting;

        public bool IsInserting
        {
            get
            {
                return _isInserting;
            }

            set
            {
                _isInserting = value;
                OnPropertyChanged("IsInserting");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
