﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Office = Microsoft.Office.Core;


// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new ElkemRibbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace ElkemBildebank
{
    [ComVisible(true)]
    public class ElkemRibbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;
        private ShapeFunctions shapeHandler;
        private BulletFormatting bulletFormatter;

        public ElkemRibbon()
        {
            shapeHandler = new ShapeFunctions();
            bulletFormatter = new BulletFormatting();
        }

        public void ShowPresBank(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.showTaskPane();
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("ElkemBildebank.ElkemRibbon.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        public void ChangeShapeColor(Office.IRibbonControl ctrl)
        {
            shapeHandler.SetColorOnShape(ctrl.Tag);

        }

        public void InsertShape(Office.IRibbonControl ctrl)
        {
            shapeHandler.DefineAndDrawShape(ctrl.Tag);

        }

        public void ToggleWatermark(Office.IRibbonControl ctrl)
        {
            shapeHandler.ToggleWatermarkVisibility(ctrl.Tag);
        }

        public void FormatBullets(Office.IRibbonControl ctrl)
        {
            bulletFormatter.FormatBullets(ctrl.Tag);
        }

        public Bitmap ButtonGetImage(Office.IRibbonControl ctrl)
        {
            switch (ctrl.Tag)
            {
                case "blaa":
                    return Properties.Resources.blaa;
                case "lysblaa":
                     return Properties.Resources.lysblaa;
                case "lysereblaa":
                     return Properties.Resources.lysereblaa;
                case "sort":
                    return Properties.Resources.sort;
                case "hvit":
                    return Properties.Resources.hvit;
                case "oransje":
                    return Properties.Resources.oransje;
                case "lysoransje":
                    return Properties.Resources.lysoransje;
                case "lilla":
                    return Properties.Resources.lilla;
                case "blackbullets":
                    return Properties.Resources.sort;
                case "bluebullets":
                    return Properties.Resources.blaa;
                case "colormenu":
                    return Properties.Resources.colors;
                default:
                    return null;
            }

            
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
