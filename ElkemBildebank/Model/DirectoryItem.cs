﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElkemBildebank.Model
{
    public class DirectoryItem : Item
    {
        private List<Item> _items = new List<Item>();

        public List<Item> Items
        {
            get
            {
                return _items;
            }

            set
            {
                _items = value;
            }
        }

        public void AddItem(string list, string path)
        {
            char[] charSeparators = new char[] { '/' };

            string strippedPath = stripSitenameFromPath(list, path);


            // Parse into a sequence of parts.
            string[] parts = strippedPath.Split(charSeparators,
                StringSplitOptions.RemoveEmptyEntries);

            // The current node.  Start with this.
            DirectoryItem current = this;

            // Iterate through the parts.
            foreach (string part in parts)
            {
                // The child node.
                Item child;


                // Does the part exist in the current node?  If
                // not, then add.
                if (current?.Items.Where(x => x.Name == part).Count() == 0)
                {
                    // Add the child.
                    // If last child, add a galleryItem. 
                    if (parts[parts.Length - 1] == part)
                    {
                        child = new GalleryItem
                        {
                            Name = part,
                            Path = path,
                            Handler = Handler
                        };
                    }
                    else
                    {
                        child = new DirectoryItem
                        {
                            Name = part, 
                            Path = path,
                            Handler = Handler
                        };
                    }

                    // Add to the dictionary.
                    current._items.Add(child);
                }
                else
                {
                    child = current?._items.Where(x => x.Name == part).FirstOrDefault();
                }

                // Set the current to the child if still inn folder.
                current = child?.GetType() == typeof(DirectoryItem) ? (DirectoryItem)child : null;
            }
        }

        private static string stripSitenameFromPath(string name, string path)
        {
            return path.Substring(path.IndexOf(name));
        }
    }
}
