﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElkemBildebank.Model
{
    public class Item
    {
        public string Name { get; set; }
        public string Path { get; set; }

        private SharepointHandler _handler;

        public SharepointHandler Handler
        {
            get
            {
                return _handler;
            }

            set
            {
                _handler = value;
            }
        }
    }
}
