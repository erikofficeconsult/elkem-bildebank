﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PPT = Microsoft.Office.Interop.PowerPoint;
using Core = Microsoft.Office.Core;
using System.Diagnostics;

namespace ElkemBildebank
{
    public class BulletFormatting
    {

        private PPT.Presentation activePres;
        private PPT.Master slideMaster;
        private PPT.TextRange masterTextrange;


        public BulletFormatting()
        {

        }


        public void FormatBullets(string tag)
        {

            try
            {
                SetProperties();


                List<PPT.TextRange> textranges = new List<PPT.TextRange>();

                foreach (PPT.Slide slide in activePres.Slides)
                {
                    foreach (PPT.Shape item in slide.Shapes.Placeholders.OfType<PPT.Shape>()
                    .Where(o => o.PlaceholderFormat.Type == PPT.PpPlaceholderType.ppPlaceholderBody || o.PlaceholderFormat.Type == PPT.PpPlaceholderType.ppPlaceholderObject))
                    {

                        if (item.HasTextFrame == Core.MsoTriState.msoTrue)
                        {
                            textranges.Add(item.TextFrame.TextRange);
                        }
                        
                    }
                }

                if (tag == "blackbullets")
                {
                    SetBulletsBlack(masterTextrange);
                    foreach (PPT.TextRange item in textranges)
                    {
                        SetBulletsBlack(item);
                    }

                }
                else
                {
                    SetBulletsBlue(masterTextrange);
                    foreach (PPT.TextRange item in textranges)
                    {
                        SetBulletsBlue(item);
                    }
                }

            }
            catch (Exception e)
            {

                Debug.WriteLine("Noe gikk galt med bulletformatting: " + e.Message);
            }
            
        }

        public void SetBulletsBlack(PPT.TextRange tr)
        {
            tr.ParagraphFormat.Bullet.Font.Color.RGB = 0;
        }

        public void SetBulletsBlue(PPT.TextRange tr)
        {
            tr.ParagraphFormat.Bullet.Font.Color.RGB = RGBToIntValue(0, 102, 166);
        }

        private void SetProperties()
        {
            try
            {
                activePres = Globals.ThisAddIn.Application.ActivePresentation;
                slideMaster = activePres.SlideMaster;

                masterTextrange = slideMaster.Shapes.OfType<PPT.Shape>()
                    .Where(o => o.PlaceholderFormat.Type == PPT.PpPlaceholderType.ppPlaceholderBody).First().TextFrame.TextRange;
            }
            catch (Exception e)
            {

                Debug.WriteLine("Noe gikk galt med bulletformat setProperties: " + e.Message);
            }
        }

        private int RGBToIntValue(int R, int G, int B)
        {
            return ((B * 256) + G) * 256 + R;
        }
    }
}
