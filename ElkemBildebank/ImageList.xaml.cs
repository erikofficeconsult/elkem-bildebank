﻿using Microsoft.SharePoint.Client;
using ElkemBildebank.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using PPT = Microsoft.Office.Interop.PowerPoint;
using System.Windows;
using System;

namespace ElkemBildebank
{
    /// <summary>
    /// Interaction logic for ImageList.xaml
    /// </summary>
    public partial class ImageList : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<ImageItem> _images;
        private ImageItem _selectedItem;
        private bool _isLoading;
        private string _folderTitle;
        public Task LoadImages { get; set; }

        public List<ImageItem> Images
        {
            get
            {
                return _images;
            }

            set
            {
                _images = value;
                OnPropertyChanged("Images");
            }
        }

        public ImageItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }

            set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        public string FolderTitle
        {
            get
            {
                return _folderTitle;
            }

            set
            {
                _folderTitle = value;
                OnPropertyChanged("FolderTitle");
            }
        }

        public ImageList()
        {
            InitializeComponent();
            DataContext = this;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }




        private void InsertCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedItem != null;
        }

        private void InsertCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InsertImage(SelectedItem.Path, SelectedItem.Name);
        }

        async void InsertImage(string path, string name)
        {
            SelectedItem.IsInserting = true;

            try
            {
                //var file = DownloadFile(path, name);
                Task<string> file = DownloadFile(path, name);
                Thread t = new Thread(() => file.GetAwaiter());
                t.IsBackground = true;
                t.Start();
                await file;
                System.IO.File.Delete(file.GetAwaiter().GetResult());

            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                Debug.WriteLine("Async Insertimage: " + e.Message);
            }

            SelectedItem.IsInserting = false;


        }

        async Task<string> DownloadFile(string path, string name)
        {

            //string filepath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var fileRef = path;
            var fileInfo = File.OpenBinaryDirect(SelectedItem.Handler.GetClientContext(), fileRef);
            var fileName = System.IO.Path.Combine(System.IO.Path.GetTempPath(), name);

            string file = await Task.Factory.StartNew(() => StartStream(fileName, fileInfo));

            try
            {
                if(System.IO.Path.GetExtension(fileRef) == ".emf")
                {
                    InsertIcon(file);
                } else
                {
                    InsertImage(file);
                }

                //PPT.Shape sh = await Task.Factory.StartNew(()
                //    => InsertImage(file));

            }
            catch (System.Runtime.InteropServices.COMException e)
            {

                Debug.WriteLine("Async Download file: "+ e.Message);
            }

            return file;
        }

        private string StartStream(string filename, FileInformation fileInfo)
        {
            using (var fileStream = System.IO.File.Create(filename))
            {
                fileInfo.Stream.CopyTo(fileStream);
                fileStream.Close();
            }
            return filename;
        }

        private void InsertIcon(string tmpFile)
        {
            try
            {
                dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;

                //sett inn icon
                Microsoft.Office.Interop.PowerPoint.Shape sh = s.Shapes.AddPicture(tmpFile, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoTrue, 0f, 0f);


                sh.Select();
                sh.ZOrder(Microsoft.Office.Core.MsoZOrderCmd.msoBringToFront);


                //plasser midt på siden
                sh.Left = (Globals.ThisAddIn.Application.ActivePresentation.PageSetup.SlideWidth / 2) - (sh.Width / 2);
                sh.Top = (Globals.ThisAddIn.Application.ActivePresentation.PageSetup.SlideHeight / 2) - (sh.Height / 2);

                //er sh plassholder

                if(sh.Type == Microsoft.Office.Core.MsoShapeType.msoPlaceholder)
                {
                    sh.Cut();
                    sh = s.Shapes.Paste()[1];
                }


                //PPT.ShapeRange shr = sh.Ungroup();

                //foreach (Microsoft.Office.Interop.PowerPoint.Shape item in shr.GroupItems)
                //{
                //    if (item.Name.IndexOf("AutoShape") != -1)
                //    {
                //        item.Delete();
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not insert image. Error: " + ex.Message);
            }
        }

        private void InsertImage(string file)
        {

            
            try
            {
                dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;
                 
                PPT.Shape placeholder = GetFirstPlaceholder(s);

                float left = Globals.ThisAddIn.Application.ActivePresentation.PageSetup.SlideWidth / 2;
                float top = Globals.ThisAddIn.Application.ActivePresentation.PageSetup.SlideHeight / 2;
                float phwidth = 0f;
                float phHeight = 0f;
                if (placeholder != null)
                {
                    left = placeholder.Left;
                    top = placeholder.Top;
                }

                bool shouldCrop = false;

                if (Globals.ThisAddIn.Application.Version != "12.0")
                {
                    if (placeholder != null)
                    {
                        shouldCrop = true;
                        phHeight = placeholder.Height;
                        phwidth = placeholder.Width;
                    }
                }


                PPT.Shape sh = s.Shapes.AddPicture(file, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoTrue, 0f, 0f);
                //PPT.Shape sh = s.Shapes.AddShape(Microsoft.Office.Core.MsoAutoShapeType.msoShapeHexagon, 0f, 0f, 285f, 285f);
                //sh.Fill.UserPicture(file);
                sh.Select();
                

                if(shouldCrop)
                {
                    sh.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse;
                    sh.Width = phwidth;
                    sh.Height = phHeight;
                    sh.Left = left;
                    sh.Top = top;
                    Globals.ThisAddIn.Application.CommandBars.ExecuteMso("PictureFillCrop");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not insert image. Error: " + ex.Message);
            }

            
        }

        private PPT.Shape GetFirstPlaceholder(PPT.Slide s)
        {
        
            foreach (PPT.Shape item in s.Shapes.Placeholders)
            {
                if (item.PlaceholderFormat.ContainedType == Microsoft.Office.Core.MsoShapeType.msoPlaceholder || item.PlaceholderFormat.ContainedType == Microsoft.Office.Core.MsoShapeType.msoAutoShape)
                {
                    if (item.PlaceholderFormat.Type == PPT.PpPlaceholderType.ppPlaceholderPicture || item.PlaceholderFormat.Type == PPT.PpPlaceholderType.ppPlaceholderObject)
                    {
                        if (item.HasTextFrame == Microsoft.Office.Core.MsoTriState.msoTrue)
                        {
                            if (item.TextFrame.TextRange.Text == "")
                            {
                                return item;
                            }
                        }
                        else
                        {
                            return item;
                        }
                        
                    }
                }
            }

            return null;
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 2)
            {
                InsertImage(SelectedItem.Path, SelectedItem.Name);
            }
        }
    }
}
