﻿using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using ElkemBildebank.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using PPT = Microsoft.Office.Interop.PowerPoint;
using System.Windows;
using System;

namespace ElkemBildebank
{
    public class DownloadHandler
    {
        private SharepointHandler handler;
        private string downloadPath = "Z:/Elkem/2017/Bildebank";

        public DownloadHandler(SharepointHandler handler)
        {
            this.handler = handler;
        }

        public void DownloadAllFiles(string path, string name, string foldername, string category)
        {
            DownloadFile(path, name, downloadPath + "/" + category + "/" + foldername + "/");
        }

        public void DownloadFile(string path, string name, string Dirpath)
        {
            var fileRef = path;
            var fileInfo = File.OpenBinaryDirect(handler.GetClientContext(), fileRef);

            bool exists = System.IO.Directory.Exists(Dirpath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Dirpath);
            }
            var fileName = System.IO.Path.Combine(Dirpath, name);

            StartStream(fileName, fileInfo);
        }

        public void LoopList(List<Item> items)
        {
            if (handler.ListName != "Logos" && handler.ListName != "Photo Library")
            {
                foreach (var item in items)
                {
                    if (item.GetType() == typeof(GalleryItem))
                    {
                        if (item.Name == "PNG")
                        {
                            // getImagesFromPath(item.Path, item.Name, handler.ListName);
                        }
                    }
                    else if (item.GetType() == typeof(DirectoryItem))
                    {
                        DirectoryItem temp = item as DirectoryItem;
                        //getImagesFromPath(item.Path, item.Name, handler.ListName);
                        LoopList(temp.Items);
                    }
                }
            }
        }

        public List<ImageItem> GetImagesFromPath(string path, string foldername, string category)
        {
            List<ImageItem> items = new List<ImageItem>();

            using (ClientContext context = handler.GetClientContext())
            {
                Folder folder = context.Web.GetFolderByServerRelativeUrl(path);
                context.Load(folder.Files);
                context.LoadQuery(folder.Files);
                context.ExecuteQuery();

                foreach (File item in GetImagesFromFolder(folder.Files))
                {
                    DownloadAllFiles(item.ServerRelativeUrl, item.Name, foldername, category);
                }
            }
            return items;
        }

        private List<File> GetImagesFromFolder(FileCollection files)
        {
            List<File> retval = new List<File>();
            List<string> restrictions = new List<string> { ".jpg", ".png", ".emf" };

            foreach (string rest in restrictions)
            {
                retval.AddRange(files.Where(x => x.Name.IndexOf(rest, StringComparison.CurrentCulture) != -1).ToList());
            }

            return retval;
        }

        private void StartStream(string filename, FileInformation fileInfo)
        {
            using (var fileStream = System.IO.File.Create(filename))
            {
                fileInfo.Stream.CopyTo(fileStream);

                fileStream.Close();
            }
        }
    }
}