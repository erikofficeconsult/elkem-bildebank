﻿using Microsoft.SharePoint.Client;
using ElkemBildebank.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Net;
using Microsoft.SharePoint.Client.Utilities;
using System.Drawing.Imaging;

namespace ElkemBildebank
{
    public class SharepointHandler
    {
        //Z:\Elkem\2017\Bildebank
        private string _siteUrl;

        private string _listName;
        private bool _photoLibrary;
        private DownloadHandler _downloader;

        public string SiteUrl
        {
            get
            {
                return _siteUrl;
            }

            set
            {
                _siteUrl = value;
            }
        }

        public string ListName
        {
            get
            {
                return _listName;
            }

            set
            {
                _listName = value;
            }
        }

        public SharepointHandler(string listname, string siteUrl, bool photoLibrary = false)
        {
            _listName = listname;
            _siteUrl = siteUrl;
            _photoLibrary = photoLibrary;
            _downloader = new DownloadHandler(this);
        }

        public List<Item> GetBildebank()
        {
            List<Item> retVal = new List<Item>();
            // First construct client context, the object which will be responsible for
            //communication with SharePoint:
            using (var context = GetClientContext())
            {
                // loop igjennom de sharepoint-listene vi skal ha
                List imagebankList = context.Web.Lists.GetByTitle(_listName);
                context.Load(imagebankList);
                context.ExecuteQuery();

                // finnes listen, og er den eventuelt tom?
                if (imagebankList != null && imagebankList.ItemCount > 0)
                {
                    var camlQuery = new CamlQuery
                    {
                        // hent alle mapper (FSObjType = 1 = Folder)
                        // som ikke har undermapper (FolderChildCount = 0)
                        ViewXml = "<View Scope='RecursiveAll'>"
                    };
                    if (!_photoLibrary)
                    {
                        camlQuery.ViewXml +=
                            @"<Query>
                                    <Where><And><Eq><FieldRef Name='FSObjType' /><Value Type='Integer'>1</Value></Eq><Eq><FieldRef Name='FolderChildCount' /><Value Type='Integer'>0</Value></Eq></And></Where>
                                </Query>";
                    }
                    else
                    {
                        camlQuery.ViewXml +=
                                @"<Query>
                                        <Where><Eq><FieldRef Name='FSObjType' /><Value Type='Integer'>1</Value></Eq></Where>
                                    </Query>";
                    }
                    camlQuery.ViewXml += @"<ViewFields><FieldRef Name='FileLeafRef' /><FieldRef Name='FileRef' /></ViewFields></View>";
                    ListItemCollection listItems = imagebankList.GetItems(camlQuery);
                    context.Load(listItems);
                    context.ExecuteQuery();

                    DirectoryItem root = new DirectoryItem() { Handler = this };

                    foreach (var item in listItems)
                    {
                        root.AddItem(_listName, item.FieldValues["FileRef"].ToString());
                    }

                    retVal = root.Items;
                    //_downloader.LoopList(root.Items);
                }
            }
            Debug.WriteLine("Ferdig: " + ListName);
            return retVal;
        }

        public List<ImageItem> GetImagesFromPath(string folder)
        {
            List<ImageItem> items = new List<ImageItem>();

            using (ClientContext clientContext = GetClientContext())
            {
                List DocumentsList = clientContext.Web.Lists.GetByTitle(_listName);
                clientContext.Load(DocumentsList);
                clientContext.ExecuteQuery();
                CamlQuery camlQuery = new CamlQuery();
                camlQuery = new CamlQuery
                {
                    ViewXml = "<View Scope='RecursiveAll'> " +
                    "<Query>" +
                    "<Where>" +
                                "<Eq>" +
                                    "<FieldRef Name='FileDirRef' />" +
                                    "<Value Type='Text'>" + folder + "</Value>" +
                                 "</Eq>" +
                    "</Where>" +
                    "</Query>" +
                    "</View>"
                };
                camlQuery.ViewXml += @"<ViewFields><FieldRef Name='FileLeafRef' /><FieldRef Name='FileRef' /></ViewFields></View>";

                ListItemCollection listItems = DocumentsList.GetItems(camlQuery);
                clientContext.Load(listItems);
                clientContext.ExecuteQuery();

                foreach (var item in listItems)
                {
                    //skip emf files
                    if (Path.GetExtension(item.FieldValues["FileRef"].ToString()) != ".emf")
                    {
                        FileInformation fileInfo = null;
                        try
                        {
                            fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, GetThumbnailPath(item.FieldValues["FileRef"].ToString()));
                        }
                        catch (WebException)
                        {
                            //thumbnail ikke funnet, bruk original
                            fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, item.FieldValues["FileRef"].ToString());
                        }
                        finally
                        {
                            if (fileInfo != null)
                            {
                                BitmapImage bitmap = new BitmapImage();
                                bitmap = ImageFromStream(fileInfo.Stream);

                                string emfPath = (Path.GetDirectoryName(item.FieldValues["FileRef"].ToString()) + "/" + Path.GetFileNameWithoutExtension(item.FieldValues["FileRef"].ToString())).Replace("\\", "/") + ".emf";
                                string fullImagePath = item.FieldValues["FileRef"].ToString();

                                //finnes det en tilsvarende emf-fil?
                                bool hasEmf = listItems.Where(x => x.FieldValues["FileRef"].ToString() == emfPath).Count() > 0;
                                string path = hasEmf ? emfPath : fullImagePath;

                                items.Add(new ImageItem()
                                {
                                    Name = item.FieldValues["FileLeafRef"].ToString(),
                                    Path = path,
                                    Image = bitmap,
                                    Handler = this
                                });
                            }
                        }
                    }
                }
            }

            return items;
        }

        public List<ImageItem> GetImagesFromPath2(string path)
        {
            List<ImageItem> items = new List<ImageItem>();

            using (ClientContext context = GetClientContext())
            {
                Folder folder = context.Web.GetFolderByServerRelativeUrl(path);
                context.Load(folder.Files, files => files.Include(file => file.Name), files => files.Include(file => file.ServerRelativeUrl));
                //context.LoadQuery(folder.Files);
                context.ExecuteQuery();

                foreach (Microsoft.SharePoint.Client.File item in GetImagesFromFolder(folder.Files))
                {
                    string itemName = StripFilename(item.Name);
                }
            }
            return items;
        }

        private string GetThumbnailPath(string path)
        {
            string folder = Path.GetDirectoryName(path);
            string filename = Path.GetFileNameWithoutExtension(path);
            string ext = Path.GetExtension(path);
            string thumbnailPath = folder + "/_t/" + filename + "_" + ext.Replace(".", "") + ext;
            return HttpUtility.UrlPathEncode(thumbnailPath.Replace("\\", "/"), true);
        }

        private List<Microsoft.SharePoint.Client.File> GetImagesFromFolder(FileCollection files)
        {
            List<Microsoft.SharePoint.Client.File> retval = new List<Microsoft.SharePoint.Client.File>();
            List<string> restrictions = new List<string> { ".jpg", ".png", ".emf" };

            foreach (string rest in restrictions)
            {
                retval.AddRange(files.Where(x => x.Name.IndexOf(rest, StringComparison.CurrentCulture) != -1).ToList());
            }

            return retval;
        }

        public ClientContext GetClientContext()
        {
            ClientContext context = new ClientContext(_siteUrl)
            {
                Credentials = GetCredentials()
            };
            return context;
        }

        private ICredentials GetCredentials()
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            SecureString passWord = new SecureString();
            foreach (char c in "PjfFdGsRu8fPVxQX".ToCharArray()) passWord.AppendChar(c);
            return new SharePointOnlineCredentials("srvimagebank@elkem.com", passWord);
        }

        private string StripFilename(string filename)
        {
            return Path.GetFileNameWithoutExtension(filename.Substring(filename.IndexOf("_", StringComparison.CurrentCulture) + 1, filename.Length - filename.IndexOf("_", StringComparison.CurrentCulture) - 1));
        }

        private void CopyStream(Stream source, Stream destination)
        {
            byte[] buffer = new byte[32768];
            int bytesRead;
            do
            {
                bytesRead = source.Read(buffer, 0, buffer.Length);
                destination.Write(buffer, 0, bytesRead);
            } while (bytesRead != 0);
        }

        private BitmapImage ImageFromStream(Stream stream)
        {
            if (stream != null)
            {
                using (var memory = new MemoryStream())
                {
                    try
                    {
                        CopyStream(stream, memory);
                        memory.Seek(0, SeekOrigin.Begin);
                        //Here you have the file in MemoryStream (memory) and you can use it
                        BitmapImage bitmap = new BitmapImage();
                        bitmap.BeginInit();
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.StreamSource = memory;
                        bitmap.EndInit();
                        bitmap.Freeze();
                        return bitmap;
                    }
                    catch (NotSupportedException)
                    {
                        return null;
                    }
                }
            }

            return null;
        }
    }
}