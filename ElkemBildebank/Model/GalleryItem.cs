﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ElkemBildebank.Model
{
    public class GalleryItem : Item
    {

        public List<ImageItem> ImageItems { get; set; }

    }
}
