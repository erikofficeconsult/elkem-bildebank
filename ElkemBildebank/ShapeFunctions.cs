﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PPT = Microsoft.Office.Interop.PowerPoint;
using Core = Microsoft.Office.Core;
using System.Diagnostics;

namespace ElkemBildebank
{
    public class ShapeFunctions
    {
        private List<ColorValue> Colors;
        private PPT.Presentation activePres;
        private PPT.Master slideMaster;
        private PPT.Shape watermarkShape;

        private string wm1 = "Internal";
        private string wm2 = "Confidential";

        public ShapeFunctions()
        {
            Colors = new List<ColorValue> {
                new ColorValue(RGBToIntValue(0,102,166), "blaa"),
                new ColorValue(RGBToIntValue(0,169,233), "lysblaa"),
                new ColorValue(RGBToIntValue(139,209,238), "lysereblaa"),
                new ColorValue(RGBToIntValue(20,23,27), "sort"),
                new ColorValue(RGBToIntValue(255,255,255), "hvit"),
                new ColorValue(RGBToIntValue(251,176,52), "lysoransje"),
                new ColorValue(RGBToIntValue(245,130,32), "oransje"),
                new ColorValue(RGBToIntValue(92, 45, 145), "lilla")
            };
        }

        public void DefineAndDrawShape(string shapeform)
        {
            SetProperties();

            try
            {
                dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;

                float CenterWidth = MiddleLeft(CM2Points(4.4)) / 2;
                float CenterHeigth = MiddleTop(CM2Points(4.4)) / 2;

                switch (shapeform)
                {
                    case "Circle":
                        PPT.Shape sh2 = s.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeOval, CenterWidth, CenterHeigth, CM2Points(4.4), CM2Points(4.4));
                        sh2.Fill.ForeColor.RGB = RGBToIntValue(0, 102, 166);
                        sh2.Line.Visible = Core.MsoTriState.msoFalse;
                        sh2.LockAspectRatio = Core.MsoTriState.msoTrue;
                        break;

                    case "Square":
                        PPT.Shape sh = s.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeRectangle, CenterWidth, CenterHeigth, CM2Points(4.4), CM2Points(4.4));
                        sh.Fill.ForeColor.RGB = RGBToIntValue(0, 102, 166);
                        sh.Line.Visible = Core.MsoTriState.msoFalse;
                        sh.LockAspectRatio = Core.MsoTriState.msoTrue;
                        break;

                    case "Hexagon":
                        CenterWidth = MiddleLeft(CM2Points(5.1)) / 2;
                        PPT.Shape sh3 = s.Shapes.AddShape(Core.MsoAutoShapeType.msoShapeHexagon, CenterWidth, CenterHeigth, CM2Points(5.1), CM2Points(4.4));
                        sh3.Fill.ForeColor.RGB = RGBToIntValue(0, 102, 166);
                        sh3.Line.Visible = Core.MsoTriState.msoFalse;
                        sh3.LockAspectRatio = Core.MsoTriState.msoTrue;
                        break;

                    case "All":
                        DrawShape(Core.MsoAutoShapeType.msoShapeOval, CenterWidth + CM2Points(4.9), CM2Points(4.4), RGBToIntValue(0, 102, 166));
                        DrawShape(Core.MsoAutoShapeType.msoShapeRectangle, CenterWidth, CM2Points(4.4), RGBToIntValue(0, 102, 166));
                        DrawShape(Core.MsoAutoShapeType.msoShapeHexagon, CenterWidth - CM2Points(5.6), CM2Points(5.1), RGBToIntValue(0, 102, 166));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("An error occured! \nPlease make sure you have the slide selected in the menu to the left and try again");
            }
        }

        public void DrawShape(Core.MsoAutoShapeType shapeType, float Left, float width, int rgb)
        {
            try
            {
                dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;

                float CenterHeigth = MiddleTop(CM2Points(4.4)) / 2;

                PPT.Shape sh = s.Shapes.AddShape(shapeType, Left, CenterHeigth, width, CM2Points(4.4));
                sh.Fill.ForeColor.RGB = rgb;
                sh.Line.Visible = Core.MsoTriState.msoFalse;
                sh.LockAspectRatio = Core.MsoTriState.msoTrue;
            }
            catch (Exception)
            {
            }
        }

        public void ToggleWatermarkVisibility(string tag)
        {
            SetProperties();
            PPT.Shape temp = null;

            try
            {
                //Delete existing
                if ((watermarkShape = slideMaster.Shapes.OfType<PPT.Shape>().FirstOrDefault(o => o.Name.IndexOf("wm_", StringComparison.CurrentCulture) != -1)) != null)
                {
                    watermarkShape.Delete();

                    foreach (PPT.CustomLayout item in activePres.SlideMaster.CustomLayouts)
                    {
                        if (item.DisplayMasterShapes == Core.MsoTriState.msoFalse)
                        {
                            try
                            {
                                if ((temp = item.Shapes.OfType<PPT.Shape>().FirstOrDefault(o => o.Name.IndexOf("wm_", StringComparison.CurrentCulture) != -1)) != null)
                                {
                                    temp.Delete();
                                }
                            }
                            catch (NullReferenceException e)
                            {
                                Debug.WriteLine("Fant ingen shapes med wm_ - navn som skal slettes: " + e.Message);
                            }
                        }
                    }
                }

                if (tag != "Delete")
                {
                    //create watermark
                    watermarkShape = DrawWatermarkTextBox(tag);

                    foreach (PPT.CustomLayout item in activePres.SlideMaster.CustomLayouts)
                    {
                        if (item.DisplayMasterShapes == Core.MsoTriState.msoFalse)
                        {
                            try
                            {
                                watermarkShape.Copy();
                                item.Shapes.Paste();
                            }
                            catch (NullReferenceException e)
                            {
                                Debug.WriteLine("Fant ingen shapes med watermark navn som skal slettes: " + e.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error in ShapeFunctions.ToggleWatermark: " + e.Message);
            }
        }

        public PPT.Shape DrawWatermarkTextBox(string content)
        {
            PPT.Shape watermark = slideMaster.Shapes.AddTextbox(Core.MsoTextOrientation.msoTextOrientationHorizontal,
                CM2Points(20),
                CM2Points(0.25),
                CM2Points(4.58),
                CM2Points(0.75));
            watermark.TextFrame.TextRange.Font.Name = "Calibri";
            watermark.TextFrame.TextRange.Font.Size = 14;
            watermark.TextFrame.TextRange.Font.Color.RGB = RGBToIntValue(54, 55, 57);
            watermark.TextFrame.TextRange.Text = "Elkem " + content;
            watermark.TextFrame.TextRange.ParagraphFormat.Alignment = PPT.PpParagraphAlignment.ppAlignRight;
            watermark.TextFrame.TextRange.ParagraphFormat.Bullet.Type = PPT.PpBulletType.ppBulletNone;
            watermark.Name = "wm_" + content;

            return watermark;
            //cal 14 rgb 54,55,57 width:4,H:0,75
        }

        public float CM2Points(double centimeters)
        {
            return (float)(centimeters * 28.5);
        }

        public float MiddleLeft(float SHwidth)
        {
            return activePres.SlideMaster.Width - SHwidth;
        }

        public float MiddleTop(float SHheight)
        {
            return activePres.SlideMaster.Height - SHheight;
        }

        public void SetColorOnShape(string colortag)
        {
            dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;
            PPT.Selection Sel = Globals.ThisAddIn.Application.ActiveWindow.Selection;
            try
            {
                if (Sel.ShapeRange.Count > 0 && Sel.ShapeRange.Type != Core.MsoShapeType.msoPlaceholder)
                {
                    int value = Colors.First(x => x.Name == colortag).RGBValue;

                    foreach (PPT.Shape selShr in Sel.ShapeRange)
                    {
                        //Handle Picture, group, auto, freeform(?)
                        switch (selShr.Type)
                        {
                            case Core.MsoShapeType.msoAutoShape:

                                selShr.Fill.ForeColor.RGB = value;

                                break;

                            case Core.MsoShapeType.msoFreeform:

                                selShr.Fill.ForeColor.RGB = value;
                                break;

                            case Core.MsoShapeType.msoGroup:

                                foreach (PPT.Shape item in selShr.GroupItems)
                                {
                                    item.Fill.ForeColor.RGB = value;
                                }
                                break;

                            case Core.MsoShapeType.msoPicture:

                                PPT.ShapeRange shr = selShr.Ungroup();

                                try
                                {
                                    foreach (PPT.Shape item in shr.GroupItems)
                                    {
                                        if (item.Name.IndexOf("AutoShape", StringComparison.CurrentCulture) != -1)
                                        {
                                            item.Delete();
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                }

                                try
                                {
                                    shr.Fill.ForeColor.RGB = value;
                                }
                                catch (Exception)
                                {
                                    s.Shapes[s.Shapes.Count].Fill.ForeColor.RGB = value;
                                }

                                break;

                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: Shapefunctions: " + e.Message);
            }
        }

        private void SetProperties()
        {
            try
            {
                activePres = Globals.ThisAddIn.Application.ActivePresentation;
                slideMaster = activePres.SlideMaster;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error in ShapeFunctions.SetProperties: " + e.Message);
            }
        }

        private int RGBToIntValue(int R, int G, int B)
        {
            return ((B * 256) + G) * 256 + R;
        }

        private void IntToRGBValues(int TotalColor)
        {
            int R = TotalColor % 256;

            int G = (TotalColor / 256) % 256;

            int B = (TotalColor / (256 * 256) % 256);
        }
    }

    public class ColorValue
    {
        public string Name;
        public int RGBValue;

        public ColorValue(int rgbValue, string name)
        {
            Name = name;
            RGBValue = rgbValue;
        }

        public ColorValue()
        {
        }

        //Old SetColorShape-metode
        //public void SetColorOnShape(string colortag)
        //{
        //    dynamic s = Globals.ThisAddIn.Application.ActiveWindow.View.Slide;
        //    PPT.Selection Sel = Globals.ThisAddIn.Application.ActiveWindow.Selection;
        //    try
        //    {
        //        if (Sel.ShapeRange.Count > 0 && Sel.ShapeRange.Type != Core.MsoShapeType.msoPlaceholder)
        //        {
        //            int value = Colors.Where(x => x.Name == colortag).First().RGBValue;

        //            foreach (PPT.Shape selShr in Sel.ShapeRange)
        //            {
        //                Debug.WriteLine("SHR Type: " + selShr.Type);

        //            }

        //            //Handle Picture, group, auto, freeform(?)
        //            switch (Sel.ShapeRange.Type)
        //            {
        //                case Core.MsoShapeType.msoAutoShape:

        //                    foreach (PPT.Shape item in Sel.ShapeRange)
        //                    {
        //                        item.Fill.ForeColor.RGB = value;
        //                    }
        //                    break;

        //                case Core.MsoShapeType.msoFreeform:

        //                    Sel.ShapeRange[1].Fill.ForeColor.RGB = value;
        //                    break;

        //                case Core.MsoShapeType.msoGroup:

        //                    PPT.ShapeRange shr2 = Sel.ShapeRange;

        //                    foreach (PPT.Shape item in shr2.GroupItems)
        //                    {
        //                        item.Fill.ForeColor.RGB = value;
        //                    }
        //                    break;

        //                case Core.MsoShapeType.msoPicture:

        //                    PPT.ShapeRange shr = Sel.ShapeRange.Ungroup();

        //                    try
        //                    {
        //                        foreach (PPT.Shape item in shr.GroupItems)
        //                        {
        //                            if (item.Name.IndexOf("AutoShape") != -1)
        //                            {
        //                                item.Delete();
        //                            }
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                    }

        //                    try
        //                    {
        //                        shr.Fill.ForeColor.RGB = value;
        //                    }
        //                    catch (Exception)
        //                    {
        //                        s.Shapes[s.Shapes.Count].Fill.ForeColor.RGB = value;
        //                    }

        //                    break;

        //                default:
        //                    break;
        //            }

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show("Error: Shapefunctions: " + e.Message);
        //    }

        //}
    }
}